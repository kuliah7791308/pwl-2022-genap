<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            'name' => 'Spidol',
            'stock' => 1000,
            'price' => 12000,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Mouse',
            'stock' => 1000,
            'price' => 50000,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'name' => 'Laptop',
            'stock' => 1000,
            'price' => 12000000,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
