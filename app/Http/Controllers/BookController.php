<?php

namespace App\Http\Controllers;

use App\Export\ExportBooks;
use App\Models\Author;
use App\Models\Book;
use App\Models\BookAuthor;
use App\Models\Publisher;
use Barryvdh\DomPDF\Facade\Pdf;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception as PHPUnitException;
use Maatwebsite\Excel\Facades\Excel;

class BookController extends Controller
{
    /**
     * Fungsi untuk menampilkan semua data books
     */
    public function index()
    {
        $books = Book::query()
            ->with(['publisher', 'authors'])
            ->when(request('search'), function ($query) {
                $searchTerm = '%' . request('search') . '%';
                $query->where('title', 'like', $searchTerm)
                    ->orWhere('code', 'like', $searchTerm)
                    ->orWhereHas('publisher', function ($query) use ($searchTerm) {
                        $query->where('name', 'like', $searchTerm);
                    })
                    ->orWhereHas('authors', function ($query) use ($searchTerm) {
                        $query->where('name', 'like', $searchTerm);
                    });
            })
            ->paginate(10);
        session()->flashInput(request()->input());
        return view('books/index', [
            'books' => $books
        ]);
    }

    public function print()
    {
        $books = Book::query()
            ->with(['publisher', 'authors'])
            ->when(request('search'), function ($query) {
                $searchTerm = '%' . request('search') . '%';
                $query->where('title', 'like', $searchTerm)
                    ->orWhere('code', 'like', $searchTerm)
                    ->orWhereHas('publisher', function ($query) use ($searchTerm) {
                        $query->where('name', 'like', $searchTerm);
                    })
                    ->orWhereHas('authors', function ($query) use ($searchTerm) {
                        $query->where('name', 'like', $searchTerm);
                    });
            })->get();
        $filename = "books_" . date('Y-m-d-H-i-s') . ".pdf";
        $pdf = Pdf::loadView('books/print', ['books' => $books]);
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream($filename);
    }


    public function printDetail($bookId)
    {
        $book = Book::findOrFail($bookId);
        $filename = "book_" . $book->code . "_" . date('Y-m-d-H-i-s') . ".pdf";
        $pdf = Pdf::loadView('books/printDetail', ['book' => $book]);
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream($filename);
    }

    /**
     * Function untuk menampilkan form tambah buku
     */
    public function create()
    {
        $publishers = Publisher::all();
        $authors = Author::all();
        return view('books/form', [
            'publishers' => $publishers,
            'authors' => $authors
        ]);
    }

    /**
     * Function untuk memproses data buku ke database
     */
    public function store(Request $request)
    {
        dd($request->all());
        DB::beginTransaction();

        try {
            $validated = $request->validate([
                'code' => 'required|max:4|unique:books,code',
                'title' => 'required|max:100',
                'id_publisher' => 'required'
            ]);

            $code = $request->code;
            $title = $request->title;
            $idPublisher = $request->id_publisher;
            $book = Book::create([
                'code' => $code,
                'title' => $title,
                'id_publisher' => $idPublisher
            ]);
            foreach ($request->author as $authorId) {
                BookAuthor::create([
                    'id_book' => $book->id,
                    'id_author' => $authorId
                ]);
            }
            DB::commit();
            return redirect(route('books.index'))->with('success', 'Buku berhasil ditambah');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect(route('books.index'))->with('errors', 'Buku Gagal ditambah');
        }
    }

    public function confirmDelete($bookId)
    {
        #ambil data buku by Id
        $book = Book::findOrFail($bookId);
        return view('books/delete-confirm', [
            'book' => $book
        ]);
    }

    public function delete(Request $request)
    {
        $bookId = $request->id;
        $book = Book::findOrFail($bookId);
        $book->delete();
        return redirect(route('books.index'))->with('success', 'Buku berhasil dihapus');
    }

    public function edit($bookId)
    {
        #ambil data buku by Id
        $book = Book::findOrFail($bookId);
        $publishers = Publisher::all();
        return view('books/form-update', [
            'book' => $book,
            'publishers' => $publishers
        ]);
    }

    public function update(Request $request)
    {
        $bookId = $request->id;
        $book = Book::findOrFail($bookId);
        $book->update([
            'title' => $request->title
        ]);
        return redirect(route('books.index'))->with('success', 'Buku berhasil diubah');
    }

    public function excel()
    {
        return Excel::download(new ExportBooks, 'books.xlsx');
    }
}
